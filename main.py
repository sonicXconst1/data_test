"""
This module contains test methods for data normalization.
"""
from typing import (
    Callable
)

import numpy as np
import matplotlib.pyplot as plt

from datatestpackage import (
    helpers)

from datatestpackage.normalization import (
    linear_values_normalization)


def main() -> None:
    RANGE_START = -5
    RANGE_STOP = 5
    RANGE_STEP = 0.1

    FROM_EXPLICIT = -1
    TO_EXPLICIT = 1

    CALCULATOR: Callable[[float], float] = helpers.parabola_at

    DATA = np.arange(RANGE_START, RANGE_STOP, RANGE_STEP)
    MAPPED_DATA = linear_values_normalization(DATA, FROM_EXPLICIT, TO_EXPLICIT)
    CALCULATED_VALUES = [CALCULATOR(value) for value in DATA]
    MAPPED_VALUES = linear_values_normalization(CALCULATED_VALUES, 0, 1)

    helpers.randomize_with_noize(CALCULATED_VALUES, -0.5, 1.0)

    FIG, (AX_LEFT, AX_RIGHT) = plt.subplots(
        nrows=1,
        ncols=2,
        sharey=True)
    AX_LEFT.plot(DATA, CALCULATED_VALUES)
    AX_RIGHT.plot(MAPPED_DATA, MAPPED_VALUES)
    plt.show()


if __name__ == "__main__":
    main()
