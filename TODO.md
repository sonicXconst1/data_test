Hello
0. The most important: each module must have test for every public function or class.
	See: https://docs.python.org/3/library/unittest.html

1. write simple least squared minimization
	- Find any reference!

2. write small encrypting and decryption system using the simplest ciphers.
	- Find any reference!

3. find a mistake in the statistics.py file, sample_variance method.
