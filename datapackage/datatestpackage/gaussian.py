"""
Simple implementation of the Gaussian function.
"""
import numpy as np

class GaussianParameters:
    """
    Simple class to represent basic parameters for the Gaussian function.

    a_value: the height of the curve's peak.
    b_value: the position of the center of the peak.
    c_value: controls the width of the "bell". The standart deviation,
             sometimes called the Gaussian RMS width.
    """

    def __init__(self, a: float = 1.0, b: float = 1.0, c: float = 1.0) -> None:
        self.__a = a
        self.__b = b
        if c < 0:
            raise ValueError("c must be > 0!")
        self.__c = c

    @property
    def a_value(self) -> float:
        """Gets the value of a"""
        return self.__a

    @property
    def b_value(self) -> float:
        """Gets the value of b"""
        return self.__b

    @property
    def c_value(self) -> float:
        """Gets the value of c"""
        return self.__c

def gaussian(parameters: GaussianParameters, point: float) -> float:
    """Calculates value using the Gaussian function in a given point."""
    _a = parameters.a_value
    _b = parameters.b_value
    _c = parameters.c_value
    return _a * np.exp(-((point - _b) ** 2) / (2 * _c**2))


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    ARGUMENTS = np.arange(-3, 3, 0.01)
    PARAMETERS = GaussianParameters(a=3, b=0, c=1.1)
    VALUES = [gaussian(PARAMETERS, argument) for argument in ARGUMENTS]
    plt.plot(ARGUMENTS, VALUES)
    plt.show()
