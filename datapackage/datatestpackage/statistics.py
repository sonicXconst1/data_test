"""
This module contains general statistics.
Each function calculates independently: it does not metter sample or
polulation is used.
"""
from typing import (
    List,
    Dict,
    NamedTuple)
from math import (
    sqrt)


__INVALID_LEN__ = "Sample is too short!"


def mean_nc(data: List[float]) -> float:
    """
    Calculates the mean of the data.
    """
    return sum(data) / len(data)


def variance_nc(data: List[float], denominator: int) -> float:
    """
    Calculates the variance in the data with given demoninator.
    """
    mean = mean_nc(data)
    return sum([(value - mean) ** 2 for value in data]) / denominator


def standard_deviation_nc(variance: float) -> float:
    """
    Calculates the standard deviation, which is the square root
    of variance.
    """
    return sqrt(variance)


def percentile_nc(data: List[float], percents: float) -> float:
    """Calculates n-th percentile of the data."""
    length = len(data)
    position = int((length + 1) * percents // 100)
    if (length + 1) * percents % 100 == 0:
        return data[position - 1]
    return mean_nc(data[position - 1:position + 1])


def median_nc(data: List[float]) -> float:
    """
    Calculates the median of the data.
    """
    return percentile_nc(data, 50)


class Quartiles(NamedTuple):
    """Tuple which represents quartiles."""
    first: float
    second: float
    third: float


def quartiles_nc(data: List[float]) -> Quartiles:
    """
    Divide a data into quarters.
    Rerurns a Quartiles (tuple of 3 floats).
    """
    return Quartiles(
        percentile_nc(data, 25),
        percentile_nc(data, 50),
        percentile_nc(data, 75))


def range_nc(data: List[float]) -> float:
    """
    Calculates the range of the data.
    """
    return max(data) - min(data)


def mode_nc(data: List[float]) -> List[float]:
    """Finds the mode of the data."""
    result: Dict[float, int] = dict()
    for value in data:
        if value in result:
            result[value] += 1
        else:
            result[value] = 0
    max_value = max(result.values())
    return [key for key, value in result.items() if value == max_value]


def trimmed_mean_nc(
        sorted_data: List[float],
        trimming_percentage: float) -> float:
    """
    Calculates trimmed mean. Percentage is calcualted by n * p / 100,
    where n is the length of the data and p is trimming percent.
    """
    multiplier = trimming_percentage / 100
    points_to_trim = round(len(sorted_data) * multiplier)
    trimmed = sorted_data[points_to_trim:-points_to_trim]
    return mean_nc(trimmed)


def standard_error_of_the_mean_nc(
        data: List[float],
        standard_deviation: float) -> float:
    """
    Calculates the standard error of the mean.

    The standard error of the mean is equal to the standard deviation
    divided by the square root of the data size.
    """
    return standard_deviation / sqrt(len(data))


def mean(sample: List[float]) -> float:
    """
    Calcultes a sample mean from the sample.

    Raises
    ------
    ValueError
        if samples langes is 0.
    """
    if sample:
        return mean_nc(sample)
    raise ValueError(__INVALID_LEN__)


def median(sorted_sample: List[float]) -> float:
    """
    Calculates sample median.
    """
    if sorted_sample:
        return median_nc(sorted_sample)
    raise ValueError(__INVALID_LEN__)


def mode(sample: List[float]) -> List[float]:
    """Finds the mode of the sample."""
    return mode_nc(sample)


def data_range(data: List[float]) -> float:
    """Calculates the range of the data."""
    if data:
        return range_nc(data)
    raise ValueError(__INVALID_LEN__)


def percentile(sorted_data: List[float], percent: float) -> float:
    """Calculates n-th percentile of the data."""
    if sorted_data:
        return percentile_nc(sorted_data, percent)
    raise ValueError(__INVALID_LEN__)


def quartiles(sorted_sample: List[float]) -> Quartiles:
    """
    Divide a sample into quarters.
    Rerurns a Quartiles (tuple of 3 floats).
    """
    if sorted_sample:
        return quartiles_nc(sorted_sample)
    raise ValueError(__INVALID_LEN__)


def trimmed_mean(
        sorted_data: List[float],
        trimming_percentage: float) -> float:
    """
    Calculates trimmed mean. Percentage is calcualted by n * p / 100,
    where n is the length of the sample and p is trimming percent.
    """
    multiplier = trimming_percentage / 100
    number_of_points_to_trim = round(len(sorted_data) * multiplier)
    if number_of_points_to_trim > 2 * len(sorted_data):
        raise ValueError(__INVALID_LEN__)
    return trimmed_mean_nc(sorted_data, trimming_percentage)
