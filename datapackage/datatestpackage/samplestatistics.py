"""
This module contains some basic functions from statistics.
"""
from typing import (
    List)
from .statistics import (
    mean_nc,
    variance_nc,
    standard_deviation_nc,
    standard_error_of_the_mean_nc)


__INVALID_LEN__ = "Sample is too short!"


def variance(sample: List[float]) -> float:
    """
    Calculates the squared sample variance.
    The sample variance is the average of the squared
    deviations, except that we divide by n - 1 instead of n.

    Raises
    ------
    ValueError
        if samples langes is 0.
    """
    if sample:
        return variance_nc(sample, len(sample) - 1)
    raise ValueError(__INVALID_LEN__)


def variance_fast(sample: List[float]) -> float:
    """
    Faster implementation of the sample mean function.

    Raises
    ------
    ValueError
        if samples langes is 0.
    """
    if sample:
        sample_mean = mean_nc(sample)
        sample_length = len(sample)
        return (sum(value * value for value in sample) -
                sample_length * sample_mean ** 2) / (sample_length - 1)
    raise ValueError(__INVALID_LEN__)


def standard_deviation(sample: List[float]) -> float:
    """
    Calculates the standard deviation of the sample.

    Raises
    ------
    ValueError
        if samples langes is 0.
    """
    return standard_deviation_nc(variance(sample))


def standard_error_of_the_mean(sample: List[float]) -> float:
    """
    Calculates the standard error of the mean.

    The standard error of the mean is equal to the standard deviation divided
    by the square root of the sample size.

    Raises
    ------
    ValueError
        if samples langes is 0.
    """
    return standard_error_of_the_mean_nc(
        sample,
        standard_deviation_nc(variance(sample)))
