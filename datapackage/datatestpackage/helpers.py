"""
Simple helpers.
"""
from typing import (
        List,
        Tuple,
        Iterable)
import random


def parabola_at(point: float) -> float:
    """Calculates simple x^2 parabola"""
    return point * point


def parabola_with_coefficients_at(
        point: float,
        coefficients: Tuple[float, float, float]) -> float:
    """Calculates parabola with given coefficients."""
    _a, _b, _c = coefficients
    return _a * point * point + _b * point + _c


def randomize_with_noize(
        values: List[float],
        starting_noise_point: float,
        noise_delta: float) -> None:
    """Adds noise to values from starting point to starting point + delta"""
    for (value_index, value) in enumerate(values):
        values[value_index] = value + random.uniform(
            starting_noise_point,
            starting_noise_point + noise_delta)


def get_sorted(collection: List[float]) -> Iterable[float]:
    """
    Sorts the collection.
    """
    result = list(collection)
    for index, item in enumerate(collection):
        for next_index, next_item in enumerate(collection[index:]):
            if item > next_item:
                result[index], result[next_index] = next_item, item
    return result
