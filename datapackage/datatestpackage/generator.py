"""
The basic data generator.
"""

from typing import (
        List,
        Callable)
import gaussian
from gaussian import (
        GaussianParameters)

def generate_with_function(
        function: Callable[[float], float],
        arguments: List[float]) -> List[float]:
    """
    Generates a list of values from the given arguments using the given function.

    function: any function, which take a float value and returns float.
    arguments: a list of arguments to pass to the function.
    """
    return [function(argument) for argument in arguments]

def gaussian_generator(
        height: float,
        position: float,
        width: float,
        arguments: List[float]) -> List[float]:
    """
    Generates gaussian function with the given parameters and arguments.

    Raises: ValueException if width is below zero.
    """
    parameters = GaussianParameters(height, position, width)
    return generate_with_function(
        lambda x: gaussian.gaussian(parameters, x),
        arguments)

if __name__ == "__main__":
    import numpy as np
    print(generate_with_function(np.sin, np.arange(-np.pi, np.pi, 0.1)))
