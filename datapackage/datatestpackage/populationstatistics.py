"""
This module represents formulas for the population statistics.
"""
from typing import (
    List)
from math import (
    sqrt)
from samplestatistics import (
    sample_mean)


def population_variance(sample: List[float]) -> float:
    """
    Calculate the squared population variance.
    """
    if sample:
        mean = sample_mean(sample)
        return sum([(value - mean)**2 for value in sample]) / len(sample)
    raise ValueError("Population has invalid length")


def population_standard_deviation(sample: List[float]) -> float:
    """
    Calculates the standard deviation of the polulation.

    Raises
    ------
    ValueError
        if samples langes is 0.
    """
    return sqrt(population_variance(sample))


def population_error_of_the_mean(sample: List[float]) -> float:
    """
    Calculates the population error of the mean.
    """
    return population_standard_deviation(sample) / len(sample)
