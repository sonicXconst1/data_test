"""
Normalization methods.
"""

from typing import (
    List)
from numpy import (
    arcsin,
    sin)


def linear_value_normalization(
        value: float,
        min_value: float,
        max_value: float,
        from_explicit: float,
        to_explicit: float) -> float:
    """
    Normalizes value in range [from_explicit, to_explicit].
                      x - min
    f(x, min, max) = ----------- (max - min) + min
                      max - min
    """
    return (value - min_value) / (max_value - min_value) \
        * (to_explicit - from_explicit) + from_explicit


def linear_values_normalization(
        values: List[float],
        from_explicit: float,
        to_explicit: float) -> List[float]:
    """
    Normalizes values in range [from_explicit, to_explicit].
    """
    min_value = min(values)
    max_value = max(values)
    return [linear_value_normalization(
        value,
        min_value,
        max_value,
        from_explicit,
        to_explicit) for value in values]


def sin_transformation_to_internal_value(
        external_value: float,
        min_value: float,
        max_value: float) -> float:
    """
    Converts internal value to external value.
    Min and max values are usually the bottom and top borders respectivly.

    ref: Function Minimization and Error Analysis, F.James, p.2
    """
    numerator = 2 * (external_value - min_value)
    denominator = max_value - min_value
    return arcsin(numerator / denominator - 1)


def sin_transformation_to_external_value(
        internal_value: float,
        min_value: float,
        max_value: float) -> float:
    """
    Converts external value to internal value.
    Min and max values are usually the bottom and top borders respectivly.

    ref: Function Minimization and Error Analysis, F.James, p.2
    """
    right_value = ((max_value - min_value) / 2) * (sin(internal_value) + 1)
    return min_value + right_value


if __name__ == "__main__":
    from numpy import arange
    from matplotlib.figure import Figure
    MIN = -10.0
    MAX = 10.0
    DELTA = 0.0
    MIN_DELTA = MIN - DELTA
    MAX_DELTA = MAX + DELTA
    INITIAL_VALUES = arange(MIN, MAX, 0.1)
    INTERNAL_VALUES = [sin_transformation_to_internal_value(value, MIN_DELTA, MAX_DELTA)
                       for value in INITIAL_VALUES]
    EXTERNAL_VALUES = [sin_transformation_to_external_value(value, MIN_DELTA, MAX_DELTA)
                       for value in INTERNAL_VALUES]
    FIGURE = Figure()
    INTERNAL_AXES = FIGURE.add_subplot(121, title="Internal")
    EXTERNAL_AXES = FIGURE.add_subplot(122, title="External")
    INTERNAL_AXES.plot(INITIAL_VALUES, INTERNAL_VALUES, "r")
    EXTERNAL_AXES.plot(INITIAL_VALUES, EXTERNAL_VALUES, "b")
    import tkinter
    import matplotlib.backends.backend_tkagg as tkagg
    WINDOW = tkinter.Tk()
    CANVAS = tkagg.FigureCanvasTkAgg(FIGURE, master=WINDOW)
    FIGURE_MANAGER = tkagg.FigureManagerTk(CANVAS, 0, WINDOW)
    FIGURE_MANAGER.set_window_title("Sin Transformation")
    FIGURE_MANAGER.resize(1000, 500)
    FIGURE_MANAGER.show()
    WINDOW.mainloop()
