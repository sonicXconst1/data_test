"""
Setup module for the linear algebra package.
"""
from setuptools import (
    setup)

setup(
    name="linear",
    version="0.0.0.1",
    packages=["linear"])

