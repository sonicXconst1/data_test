"""
Runs available tests.
"""
import unittest

LOADER = unittest.TestLoader()

START_DIR = "tests/"
PATTERN = "*_tests.py"

SUITE = LOADER.discover(START_DIR, pattern=PATTERN)

RUNNER = unittest.TextTestRunner(verbosity=2)
RUNNER.run(SUITE)
