"""
Test module for the gaussian.py module.
"""
import unittest
from datatestpackage import (
    gaussian)


class GaussianModuleTestCase(unittest.TestCase):
    """
    Test case for the gaussian.py module.
    """

    def test_coefficients(self) -> None:
        """
        Test GaussianCoefficients with invalid values.
        """
        _parameters = gaussian.GaussianParameters(a=-1.0, b=-1.0)
        self.assertNotEqual(_parameters, None)
        with self.assertRaises(ValueError):
            gaussian.GaussianParameters(c=-1.0)

    def test_invalid_arguments(self):
        """
        Tests the Gaussian function with some invalid arguments.
        Cases:
            - 0 amount of the coefficients.
            - investigate edge cases for float values in python
              and tests for them (float.min_value, float.NaN etc.)
        """
        pass

    def test_range(self):
        pass


if __name__ == "__main__":
    unittets.main()
