"""
Simple statistics methods tests.
"""
from unittest import (
    TestCase)
from math import (
    sqrt)
from datatestpackage.statistics import (
    mean,
    median,
    trimmed_mean,
    data_range,
    mode,
    quartiles,
    percentile)
from datatestpackage.samplestatistics import (
    variance,
    variance_fast,
    standard_deviation,
    standard_error_of_the_mean)


class StatisticsTest(TestCase):
    """
    Tests for the statistics.py module.
    """

    def test_sample_mean(self):
        """
        Simple test with sample mean
        """
        values = [19, 20, 21]
        self.assertAlmostEqual(mean(values), 20.0)

    def test_sample_variance(self) -> None:
        """
        Simple test for sample variance.
        """
        values = [20, 22, 24]
        sample_variance = variance(values)
        self.assertAlmostEqual(sample_variance, 4.0)

    def test_sample_variance_fast(self) -> None:
        """
        Simple test for sample variance.
        """
        values = [20, 22, 24]
        sample_variance = variance_fast(values)
        self.assertAlmostEqual(sample_variance, 4.0)

    def test_standard_deviation(self) -> None:
        """
        Simple test for standard deviation.
        """
        values = [20.0, 22.0, 24.0]
        sample_variance: float = variance_fast(values)
        sample_standard_deviation: float = standard_deviation(values)
        self.assertAlmostEqual(
            sqrt(sample_variance),
            sample_standard_deviation,
            delta=1e-2)

    def test_standard_error_of_the_mean(self) -> None:
        """
        Standard error of the mean test.
        """
        values = [20, 24, 28]
        sample_standard_deviation = standard_deviation(values)
        self.assertAlmostEqual(
            sample_standard_deviation / sqrt(len(values)),
            standard_error_of_the_mean(values))

    def test_standard_deviation_with_multiplication(self) -> None:
        """
        Test for the multiplication of standard deviation.
        """
        values = [10, 20, 30, 40]
        factor = 5
        multiplied_values = [value * factor for value in values]
        self.assertAlmostEqual(
            standard_deviation(values),
            standard_deviation(multiplied_values) / factor)

    def test_sample_median(self) -> None:
        """
        Tests sample median.
        """
        sample = [1, 2, 3]
        self.assertAlmostEqual(median(sample), 2)
        sample.append(4)
        self.assertAlmostEqual(median(sample), mean([2, 3]))
        sample = [1]
        self.assertAlmostEqual(median(sample), 1)

    def test_trimmed_mean(self) -> None:
        """
        Tests trimmed_mean.
        """
        sample = [30, 75, 79, 80, 80, 105, 126, 138, 149, 179, 179, 191,
                  223, 232, 232, 236, 240, 242, 245, 247, 254, 274, 384, 470]
        five_percent_mean = trimmed_mean(sample, 5)
        self.assertAlmostEqual(190.45, five_percent_mean, delta=1e-2)
        ten_percent_mean = trimmed_mean(sample, 10)
        self.assertAlmostEqual(186.55, ten_percent_mean, delta=1e-2)
        twenty_percent_mean = trimmed_mean(sample, 20)
        self.assertAlmostEqual(194.07, twenty_percent_mean, delta=1e-2)

    def test_sample_range(self) -> None:
        """
        Tests sample range.
        """
        sample = [30, 75, 79, 80, 80, 105, 126, 138, 149, 179, 179, 191,
                  223, 232, 232, 236, 240, 242, 245, 247, 254, 274, 384, 470]
        test_range = data_range(sample)
        self.assertAlmostEqual(440.0, test_range, delta=1e-1)

    def test_sample_mode(self) -> None:
        """
        Tests sample range.
        """
        sample = [30, 75, 79, 80, 80, 105, 126, 138, 149, 179, 179, 191,
                  223, 232, 232, 236, 240, 242, 245, 247, 254, 274, 384, 470]
        modes = mode(sample)
        self.assertEqual([80.0, 179.0, 232.0], modes)

    def test_odd_sample_quartile(self) -> None:
        """Tests splitting odd sample into 4 parts."""
        sample = [30, 75, 79, 80, 80, 105, 126, 138, 149, 179, 179, 191,
                  223, 232, 232, 236, 240, 242, 245, 247, 254, 274, 384, 470]
        sample_quartiles = quartiles(sample)
        self.assertEqual(sample_quartiles, (115.5, median(sample), 243.5))

    def test_even_sample_quartile(self) -> None:
        """Tests splitting event sample into 4 parts."""
        sample = [7.0, 15.0, 36.0, 39.0, 40.0, 41.0]
        sample_quartiles = quartiles(sample)
        self.assertEqual(sample_quartiles, (11.0, 37.5, 40.5))

    def test_percentiles(self) -> None:
        """Tests percentile method."""
        sample = [30, 75, 79, 80, 80, 105, 126, 138, 149, 179, 179, 191,
                  223, 232, 232, 236, 240, 242, 245, 247, 254, 274, 384, 470]
        self.assertEqual(percentile(sample, 65), 238)
