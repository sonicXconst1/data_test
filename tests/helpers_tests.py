"""
Test cases for the helpers.
"""
from unittest import (
    TestCase)
from datatestpackage.helpers import (
    get_sorted)


class HelpersTests(TestCase):
    """
    Test cases for the helpers.
    """

    def get_sorted_test(self) -> None:
        """
        Simple sorting test.
        """
        samples = [
            [3, 2, 1],
            [1],
            [1, 2, 1],
            [1, 2, 4, 2, 3, 1]]
        for sample in samples:
            self.assertAreEqual(get_sorted(sample), sorted(sample))
