"""
Test case for the normalization.
"""
from unittest import (
    TestCase)
from datatestpackage.normalization import (
    sin_transformation_to_internal_value,
    sin_transformation_to_external_value)
from numpy import (
    arange,
    errstate,
    pi)


class NormalizationTest(TestCase):
    """
    Test case for the normalization methods from the normalization.py module.

    sin transformation:

        external-to-internal: arcsin function.
        internal-to-external: sin function.
    """

    def test_lin_transformation_out_of_range(self):
        """
        Tests gettings vlaue out of normalization range.
        """
        pass

    def test_lin_transformation_in_range(self):
        """
        Tests getting values from internal to external.
        """
        pass

    def test_sin_transformation_out_of_range(self):
        """
        Tests getting values out of normalization range.
        """
        min_value = -5.0
        max_value = 5.0
        value_below_min = -6.0
        value_above_max = 6
        with errstate(all='raise'):
            with self.assertRaises(FloatingPointError):
                sin_transformation_to_internal_value(
                    value_below_min,
                    min_value,
                    max_value)
            with self.assertRaises(FloatingPointError):
                sin_transformation_to_internal_value(
                    value_above_max,
                    min_value,
                    max_value)
        # no exception expected
        self.assertTrue(
            sin_transformation_to_external_value(
                value_below_min,
                min_value,
                max_value) is not None)
        self.assertTrue(
            sin_transformation_to_external_value(
                value_above_max,
                min_value,
                max_value) is not None)

    def test_in_range_sin_external_to_internal(self) -> None:
        """
        Tests is in range external-to-internal sin transformation valid or not.
        """
        epsilon = 1e-6
        max_value = 1.0
        min_value = -max_value
        # min value test.
        value = min_value
        self.assertAlmostEqual(
            sin_transformation_to_internal_value(value, min_value, max_value),
            -pi / 2.0,
            delta=epsilon)
        # max value test.
        value = max_value
        self.assertAlmostEqual(
            sin_transformation_to_internal_value(value, min_value, max_value),
            pi / 2.0,
            delta=epsilon)

    def test_in_range_sin_internal_to_external(self) -> None:
        """
        Tests in range internal-to-external sin transformation.
        """
        epsilon = 1e-6
        max_value = 1.0
        min_value = -max_value
        # max_value_test.
        value = pi / 2.0
        self.assertAlmostEqual(
            sin_transformation_to_external_value(value, min_value, max_value),
            max_value,
            delta=epsilon)
        # min value test.
        value = -pi / 2.0
        self.assertAlmostEqual(
            sin_transformation_to_external_value(value, min_value, max_value),
            min_value,
            delta=epsilon)
        self.assertAlmostEqual(
            sin_transformation_to_external_value(0, min_value, max_value),
            0.0,
            delta=epsilon)
